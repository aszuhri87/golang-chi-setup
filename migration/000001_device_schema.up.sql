CREATE TABLE devices (
   id UUID primary key,
   name VARCHAR(255),
   description TEXT,
   pins TEXT,
   image VARCHAR(255),
   created_at TIMESTAMP default now(),
   updated_at TIMESTAMP default now(),
   deleted_at TIMESTAMP default now()
);
