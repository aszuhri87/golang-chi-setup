CREATE TABLE timers (
   id UUID primary key,
   start_time TIMESTAMP,
   end_time TIMESTAMP,
   created_at TIMESTAMP default now(),
   updated_at TIMESTAMP default now(),
   deleted_at TIMESTAMP default now()
);
