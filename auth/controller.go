package auth

import (
	//...
	// import the jwt-go library
	"chi/config"
	"chi/utils"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/go-chi/jwtauth/v5"
	"github.com/google/uuid"
	//...
)

// Register godoc
// @Summary      Register User
// @Description  Register bro
// @Tags         register
// @Accept       json
// @Produce      json
// @Param data body auth.Create true "The input todo struct"
// @Success      200  {object}  auth.DataResponseOk
// @Failure      400  {object}  utils.ErrorResponse
// @Failure      404  {object}  utils.ErrorResponse
// @Failure      500  {object}  utils.ErrorResponse
// @Router       /register [post]
func Register(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var u User

	json.NewDecoder(r.Body).Decode(&u)

	hash, _ := utils.HashPassword(u.Password)

	token := config.MakeToken(u.ID)

	fmt.Println(u.Password)
	fmt.Println(hash)
	fmt.Println(token)

	input := User{ID: uuid.New(), Name: u.Name, Username: u.Username, Password: hash}

	config.DB.Create(&input)

	data := ResponseToken{Name: u.Name, Username: u.Username, Token: "Bearer " + token}

	res := DataResponseOk{Data: data, Message: "OK", Code: http.StatusOK}
	err := json.NewEncoder(w).Encode(res)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

// Login godoc
// @Summary      Login User
// @Description  Login bro
// @Tags         Login
// @Accept       json
// @Produce      json
// @Param username query string true "Username"
// @Param password query string true "Password"
// @Success      200  {object}  auth.DataResponseOk
// @Failure      400  {object}  auth.ResponseStatus
// @Failure      404  {object}  auth.ResponseStatus
// @Failure      500  {object}  auth.ResponseStatus
// @Router       /login [post]
func Login(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	username := r.URL.Query().Get("username")
	login_password := r.URL.Query().Get("password")

	var user = User{Username: username}
	config.DB.First(&user)

	password := utils.CheckPasswordHash(login_password, user.Password)

	if !password {
		json.NewEncoder(w).Encode(ResponseStatus{Message: "Unauthorized", Code: http.StatusUnauthorized})
		return
	}

	token := config.MakeToken(user.ID)

	expirationTime := time.Now().Add(24 * time.Hour)

	data := ResponseToken{Name: user.Name, Username: username, Token: "Bearer " + token}

	res := DataResponseOk{Data: data, Message: "OK", Code: http.StatusOK}
	err := json.NewEncoder(w).Encode(res)

	http.SetCookie(w, &http.Cookie{
		Name:    "token",
		Value:   token,
		Expires: expirationTime,
	})

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

// Logout godoc
// @Security Bearer
// @Summary      Logout
// @Description  logout bro
// @Tags         Auth
// @Accept       json
// @Produce      json
// @Success      200  {object}  auth.ResponseStatus
// @Failure      400  {object}  auth.ResponseStatus
// @Failure      404  {object}  auth.ResponseStatus
// @Failure      500  {object}  auth.ResponseStatus
// @Router       /logout [get]
func Logout(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	jwtauth.FromContext(r.Context())

	http.SetCookie(w, &http.Cookie{
		Name:    "token",
		Expires: time.Now(),
	})

	result := ResponseStatus{Message: "OK", Code: http.StatusOK}
	err := json.NewEncoder(w).Encode(result)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
