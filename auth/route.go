package auth

import (
	"github.com/go-chi/chi/v5"
)

func AuthRoute(route *chi.Mux) *chi.Mux {

	route.Group(func(r chi.Router) {
		// r.Use(jwtauth.Verifier(config.JWT()))
		// r.Use(jwtauth.Authenticator)

		r.Post("/register", Register)
		r.Post("/login", Login)
		r.Get("/logout", Logout)

	})

	return route
}
