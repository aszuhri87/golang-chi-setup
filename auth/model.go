package auth

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
)

type Credentials struct {
	Password string `json:"password"`
	Username string `json:"username"`
}

type User struct {
	gorm.Model
	ID       uuid.UUID `gorm:"column:id; gorm:"type:uuid;default:uuid_generate_v4()" json:"id"`
	Name     string    `gorm:"column:name;comment:''" json:"name"`
	Username string    `gorm:"column:username;comment:''" json:"username"`
	Password string    `gorm:"column:password;comment:''" json:"password"`
}

type Create struct {
	Name     string `gorm:"column:name;comment:''" json:"name"`
	Username string `gorm:"column:username;comment:''" json:"username"`
	Password string `gorm:"column:password;comment:''" json:"password"`
}

type LoginSchema struct {
	Password string `gorm:"column:password;comment:''" json:"password"`
	Username string `gorm:"column:username;comment:''" json:"username"`
}

var users = map[string]string{
	"user1": "password1",
	"user2": "password2",
}

type ListResponseOk struct {
	Data    []User `json:data`
	Message string `json:"message"`
	Code    int    `json:"code"`
}

type DataResponseOk struct {
	Data    ResponseToken `json:data`
	Message string        `json:"message"`
	Code    int           `json:"code"`
}

type ResponseStatus struct {
	Message string `json:"message"`
	Code    int    `json:"code"`
}

type ResponseToken struct {
	Token    string `json:"token"`
	Name     string `json:"name"`
	Username string `json:"username"`
}
