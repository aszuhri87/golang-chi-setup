package config

import (
	"github.com/go-chi/jwtauth/v5"
	"github.com/google/uuid"
)

var tokenAuth *jwtauth.JWTAuth

func JWT() *jwtauth.JWTAuth {
	tokenAuth = jwtauth.New("HS256", []byte("d8789236abac4eb24b4a5fc301be5053347ac4c9f91d3f2ac525336e7967f4c3"), nil) // replace with secret key

	return tokenAuth
	// For debugging/example purposes, we generate and print
	// a sample jwt token with claims `user_id:123` here:
	// _, tokenString, _ := tokenAuth.Encode(map[string]interface{}{"user_id": 123})
	// fmt.Printf("DEBUG: a sample jwt is %s\n\n", tokenString)
}

func MakeToken(id uuid.UUID) string {
	_, tokenString, _ := tokenAuth.Encode(map[string]interface{}{"id": id})

	return tokenString
}
