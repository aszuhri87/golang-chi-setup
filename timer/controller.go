package timer

import (
	"chi/config"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/google/uuid"
)

// ShowDevice godoc
// @Summary      Show all timer
// @Description  get string by ID
// @Tags         timer
// @Accept       json
// @Produce      json
// @Success      200  {object}  device.ListResponseOk
// @Failure      400  {object}  utils.ErrorResponse
// @Failure      404  {object}  utils.ErrorResponse
// @Failure      500  {object}  utils.ErrorResponse
// @Router       /timer [get]
func Get(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	timer1 := time.NewTimer(1 * time.Minute)
	start := time.Now()

	<-timer1.C
	fmt.Println("Timer 1 fired")

	timer2 := time.NewTimer(time.Minute)
	go func() {
		<-timer2.C
		fmt.Println("Timer 2 fired")
	}()
	stop2 := timer2.Stop()
	var end time.Time
	if stop2 {
		end = time.Now()
		fmt.Println("Timer 2 stopped")
		fmt.Println("YYYY-MM-DD hh:mm:ss : ", time.Now())
	}

	fmt.Println(stop2)

	time.Sleep(2 * time.Second)

	dev := Timer{ID: uuid.New(), Start_time: start, End_time: end}

	config.DB.Create(&dev)

	var timer_data = Create{Start_time: start, End_time: end}

	result := DataResponseOk{Data: timer_data, Message: "OK", Code: http.StatusOK}
	err := json.NewEncoder(w).Encode(result)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
