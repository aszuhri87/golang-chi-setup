package timer

import (
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

type Timer struct {
	gorm.Model
	ID         uuid.UUID `gorm:"column:id; gorm:"type:uuid;default:uuid_generate_v4()" json:"id"`
	Start_time time.Time `gorm:"column:start_time;comment:''" json:"start_time"`
	End_time   time.Time `gorm:"column:end_time;comment:''" json:"end_time"`
}

type Create struct {
	Start_time time.Time `gorm:"column:start_time;comment:''" json:"start_time"`
	End_time   time.Time `gorm:"column:end_time;comment:''" json:"end_time"`
}

type ListResponseOk struct {
	Data    []Timer `json:data`
	Message string  `json:"message"`
	Code    int     `json:"code"`
}

type DataResponseOk struct {
	Data    Create `json:data`
	Message string `json:"message"`
	Code    int    `json:"code"`
}

type ResponseStatus struct {
	Message string `json:"message"`
	Code    int    `json:"code"`
}
