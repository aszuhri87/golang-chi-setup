package timer

import (
	"github.com/go-chi/chi/v5"
)

func TimerRoute(route *chi.Mux) *chi.Mux {

	route.Group(func(r chi.Router) {
		// r.Use(jwtauth.Verifier(config.JWT()))
		// r.Use(jwtauth.Authenticator)

		r.Get("/timer", Get)
	})

	return route
}
