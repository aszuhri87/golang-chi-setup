package main

import (
	"chi/auth"
	"chi/timer"
	"chi/user"
	"fmt"
	"net/http"

	"chi/config"
	"chi/device"

	_ "chi/docs"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	_ "github.com/lib/pq"
	httpSwagger "github.com/swaggo/http-swagger"
)

var router = chi.NewRouter()

// / @title chi-swagger example APIs
// @version 1.0
// @description chi-swagger example APIs
// @BasePath /

// @securityDefinitions.apikey Bearer
// @in header
// @name Authorization
// @description use `Bearer <xx token xx>` to authenticate

func main() {
	config.Conn()

	//middleware
	router.Use(middleware.Logger)
	router.Use(middleware.Recoverer)

	//router definition
	device.DeviceRoute(router)
	user.UserRoute(router)
	auth.AuthRoute(router)
	timer.TimerRoute(router)

	//documentation
	router.Mount("/docs", httpSwagger.WrapHandler)

	fmt.Println("Server started!")
	err := http.ListenAndServe(":8090", router)
	if err != nil {
		fmt.Printf("Error starting server: %s", err)
	}
}
