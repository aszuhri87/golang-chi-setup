package user

import (
	"chi/config"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/jwtauth/v5"
)

func UserRoute(route *chi.Mux) *chi.Mux {

	route.Group(func(r chi.Router) {
		r.Use(jwtauth.Verifier(config.JWT()))
		r.Use(jwtauth.Authenticator)

		r.Get("/user", Get)
		r.Get("/user/{id}", GetFirst)
		r.Post("/user", Post)
		r.Put("/user/{id}", Put)
		r.Delete("/user/{id}", Delete)
	})
	route.Get("/package", Package)

	return route
}
