package user

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	ID       uuid.UUID `gorm:"column:id; gorm:"type:uuid;default:uuid_generate_v4()" json:"id"`
	Name     string    `gorm:"column:name;comment:''" json:"name"`
	Username string    `gorm:"column:username;comment:''" json:"username"`
	Password string    `gorm:"column:password;comment:''" json:"password"`
}

type Packag struct {
	Id    string `json:"id"`
	Name  string `json:"name"`
	Price string `json:"price"`
}

type Pack struct {
	Code    int      `json:"code"`
	Message string   `json:"message"`
	Data    []Packag `json:"data"`
}

type Create struct {
	Name     string `gorm:"column:name;comment:''" json:"name"`
	Username string `gorm:"column:username;comment:''" json:"username"`
	Password string `gorm:"column:password;comment:''" json:"password"`
}

type ListResponseOk struct {
	Data    []User `json:"data"`
	Message string `json:"message"`
	Code    int    `json:"code"`
}

type List2ResponseOk struct {
	Data    string `json:"data"`
	Message string `json:"message"`
	Code    int    `json:"code"`
}

type DataResponseOk struct {
	Data    User   `json:"data"`
	Message string `json:"message"`
	Code    int    `json:"code"`
}

type StatusOk struct {
	Message string `json:"message"`
	Code    int    `json:"code"`
}
