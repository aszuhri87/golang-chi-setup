package user

import (
	"chi/config"
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/jwtauth/v5"

	"github.com/google/uuid"
)

// ShowUser godoc
// @Security Bearer
// @Summary      Show all users
// @Description  get string by ID
// @Tags         users
// @Accept       json
// @Produce      json
// @Success      200  {object}  user.ListResponseOk
// @Failure      400  {object}  utils.ErrorResponse
// @Failure      404  {object}  utils.ErrorResponse
// @Failure      500  {object}  utils.ErrorResponse
// @Router       /user [get]
func Get(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	jwtauth.FromContext(r.Context())

	var user []User
	config.DB.Find(&user)

	result := ListResponseOk{Data: user, Message: "OK", Code: http.StatusOK}
	err := json.NewEncoder(w).Encode(result)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

// ShowUserFirst godoc
// @Security Bearer
// @Summary      Show an User By ID
// @Description  get User by ID
// @Tags         users
// @Accept       json
// @Produce      json
// @Param id path string true "User ID"
// @Success      200  {object}  user.DataResponseOk
// @Failure      400  {object}  utils.ErrorResponse
// @Failure      404  {object}  utils.ErrorResponse
// @Failure      500  {object}  utils.ErrorResponse
// @Router       /user/{id} [get]
func GetFirst(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	jwtauth.FromContext(r.Context())

	id := uuid.MustParse(chi.URLParam(r, "id"))

	var user = User{ID: id}
	config.DB.First(&user)

	result := DataResponseOk{Data: user, Message: "OK", Code: http.StatusOK}
	err := json.NewEncoder(w).Encode(result)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

// CreateUser godoc
// @Security Bearer
// @Summary      Create an User
// @Description  create
// @Tags         users
// @Accept       json
// @Produce      json
// @Param data body user.Create true "The input todo struct"
// @Success      200  {object}  user.DataResponseOk
// @Failure      400  {object}  utils.ErrorResponse
// @Failure      404  {object}  utils.ErrorResponse
// @Failure      500  {object}  utils.ErrorResponse
// @Router       /user [post]
func Post(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	jwtauth.FromContext(r.Context())

	var d User

	json.NewDecoder(r.Body).Decode(&d)

	// dev := User{ID: uuid.New(), Name: d.Name, Description: d.Description, Pins: d.Pins, Image: d.Image}

	// config.DB.Create(&dev)

	res := DataResponseOk{Data: d, Message: "OK", Code: http.StatusOK}
	err := json.NewEncoder(w).Encode(res)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

// UpdateUser godoc
// @Security Bearer
// @Summary      Update an User
// @Description  Update
// @Tags         users
// @Accept       json
// @Produce      json
// @Param id path string true "User ID"
// @Param data body user.Create true "The input todo struct"
// @Success      200  {object}  user.DataResponseOk
// @Failure      400  {object}  utils.ErrorResponse
// @Failure      404  {object}  utils.ErrorResponse
// @Failure      500  {object}  utils.ErrorResponse
// @Router       /user/{id} [put]
func Put(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	id := uuid.MustParse(chi.URLParam(r, "id"))

	jwtauth.FromContext(r.Context())

	var d User
	config.DB.Where("id = ?", id).First(&d)

	json.NewDecoder(r.Body).Decode(&d)
	// edit := User{Name: d.Name, Description: d.Description, Pins: d.Pins, Image: d.Image}

	// config.DB.Model(&d).Updates(&edit)

	res := DataResponseOk{Data: d, Message: "OK", Code: http.StatusOK}
	err := json.NewEncoder(w).Encode(res)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

// DeleteUser godoc
// @Security Bearer
// @Summary      Delete an User
// @Description  Delete
// @Tags         users
// @Accept       json
// @Produce      json
// @Param id path string true "User ID"
// @Success      200  {object}  user.StatusOk
// @Failure      400  {object}  utils.ErrorResponse
// @Failure      404  {object}  utils.ErrorResponse
// @Failure      500  {object}  utils.ErrorResponse
// @Router       /user/{id} [delete]
func Delete(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	jwtauth.FromContext(r.Context())

	id := uuid.MustParse(chi.URLParam(r, "id"))

	config.DB.Delete(&User{}, id)

	res := StatusOk{Message: "OK", Code: http.StatusOK}
	err := json.NewEncoder(w).Encode(res)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

// ShowUser godoc
// @Summary      Show all packages
// @Description  get string by ID
// @Tags         packages
// @Accept       json
// @Produce      json
// @Success      200  {object}  user.List2ResponseOk
// @Failure      400  {object}  utils.ErrorResponse
// @Failure      404  {object}  utils.ErrorResponse
// @Failure      500  {object}  utils.ErrorResponse
// @Router       /package [get]
func Package(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	var packages Pack

	webhookURL := "https://api.kelip.exapps.dev/package"

	resp, errs := http.Get(webhookURL)
	if errs != nil {
		panic(errs)
	}

	defer resp.Body.Close()
	body, errs := ioutil.ReadAll(resp.Body)

	if errs != nil {
		panic(errs)
	}

	resx := body
	resx_err := json.Unmarshal(resx, &packages)
	if resx_err != nil {
		panic(resx_err)
	}

	result := packages

	err := json.NewEncoder(w).Encode(result)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
