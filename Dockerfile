FROM golang:alpine

RUN apk update && apk add --no-cache git && apk add --no-cach bash && apk add build-base

WORKDIR /app

RUN go install github.com/cosmtrek/air@latest

COPY . .

EXPOSE 8090

RUN go mod tidy

RUN go build 

CMD [ "go", "run main.go" ] 
