package device

import (
	"chi/config"
	"encoding/json"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/jwtauth/v5"

	"github.com/google/uuid"
)

// ShowDevice godoc
// @Security Bearer
// @Summary      Show all devices
// @Description  get string by ID
// @Tags         devices
// @Accept       json
// @Produce      json
// @Success      200  {object}  device.ListResponseOk
// @Failure      400  {object}  utils.ErrorResponse
// @Failure      404  {object}  utils.ErrorResponse
// @Failure      500  {object}  utils.ErrorResponse
// @Router       /device [get]
func Get(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	jwtauth.FromContext(r.Context())

	var device []Device
	config.DB.Find(&device)

	result := ListResponseOk{Data: device, Message: "OK", Code: http.StatusOK}
	err := json.NewEncoder(w).Encode(result)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

// ShowDeviceFirst godoc
// @Security Bearer
// @Summary      Show an Device By ID
// @Description  get Device by ID
// @Tags         devices
// @Accept       json
// @Produce      json
// @Param id path string true "Device ID"
// @Success      200  {object}  device.DataResponseOk
// @Failure      400  {object}  utils.ErrorResponse
// @Failure      404  {object}  utils.ErrorResponse
// @Failure      500  {object}  utils.ErrorResponse
// @Router       /device/{id} [get]
func GetFirst(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	jwtauth.FromContext(r.Context())

	id := uuid.MustParse(chi.URLParam(r, "id"))

	var device = Device{ID: id}
	config.DB.First(&device)

	result := DataResponseOk{Data: device, Message: "OK", Code: http.StatusOK}
	err := json.NewEncoder(w).Encode(result)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

// CreateDevice godoc
// @Security Bearer
// @Summary      Create an Device
// @Description  create
// @Tags         devices
// @Accept       json
// @Produce      json
// @Param data body device.Create true "The input todo struct"
// @Success      200  {object}  device.DataResponseOk
// @Failure      400  {object}  utils.ErrorResponse
// @Failure      404  {object}  utils.ErrorResponse
// @Failure      500  {object}  utils.ErrorResponse
// @Router       /device [post]
func Post(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")

	jwtauth.FromContext(r.Context())

	var d Device

	json.NewDecoder(r.Body).Decode(&d)

	dev := Device{ID: uuid.New(), Name: d.Name, Description: d.Description, Pins: d.Pins, Image: d.Image}

	config.DB.Create(&dev)

	res := DataResponseOk{Data: d, Message: "OK", Code: http.StatusOK}
	err := json.NewEncoder(w).Encode(res)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

// UpdateDevice godoc
// @Security Bearer
// @Summary      Update an Device
// @Description  Update
// @Tags         devices
// @Accept       json
// @Produce      json
// @Param id path string true "Device ID"
// @Param data body device.Create true "The input todo struct"
// @Success      200  {object}  device.DataResponseOk
// @Failure      400  {object}  utils.ErrorResponse
// @Failure      404  {object}  utils.ErrorResponse
// @Failure      500  {object}  utils.ErrorResponse
// @Router       /device/{id} [put]
func Put(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	id := uuid.MustParse(chi.URLParam(r, "id"))

	jwtauth.FromContext(r.Context())

	var d Device
	config.DB.Where("id = ?", id).First(&d.ID)

	json.NewDecoder(r.Body).Decode(&d)
	edit := Device{Name: d.Name, Description: d.Description, Pins: d.Pins, Image: d.Image}

	config.DB.Model(&d).Updates(&edit)

	res := DataResponseOk{Data: d, Message: "OK", Code: http.StatusOK}
	err := json.NewEncoder(w).Encode(res)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

// DeleteDevice godoc
// @Security Bearer
// @Summary      Delete an Device
// @Description  Delete
// @Tags         devices
// @Accept       json
// @Produce      json
// @Param id path string true "Device ID"
// @Success      200  {object}  device.ResponseStatus
// @Failure      400  {object}  utils.ErrorResponse
// @Failure      404  {object}  utils.ErrorResponse
// @Failure      500  {object}  utils.ErrorResponse
// @Router       /device/{id} [delete]
func Delete(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	jwtauth.FromContext(r.Context())

	id := uuid.MustParse(chi.URLParam(r, "id"))

	config.DB.Delete(&Device{}, id)

	res := ResponseStatus{Message: "OK", Code: http.StatusOK}
	err := json.NewEncoder(w).Encode(res)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
