package device

import (
	"github.com/google/uuid"
	"gorm.io/gorm"
)

type Device struct {
	gorm.Model
	ID          uuid.UUID `gorm:"column:id; gorm:"type:uuid;default:uuid_generate_v4()" json:"id"`
	Name        string    `gorm:"column:name;comment:''" json:"name"`
	Description string    `gorm:"column:description;comment:''" json:"description"`
	Pins        string    `gorm:"column:pins;comment:''" json:"pins"`
	Image       string    `gorm:"column:image;comment:''" json:"image"`
}

type Create struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Pins        string `json:"pins"`
	Image       string `json:"image"`
}

type ListResponseOk struct {
	Data    []Device `json:data`
	Message string   `json:"message"`
	Code    int      `json:"code"`
}

type DataResponseOk struct {
	Data    Device `json:data`
	Message string `json:"message"`
	Code    int    `json:"code"`
}

type ResponseStatus struct {
	Message string `json:"message"`
	Code    int    `json:"code"`
}
