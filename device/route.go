package device

import (
	"chi/config"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/jwtauth/v5"
)

func DeviceRoute(route *chi.Mux) *chi.Mux {

	route.Group(func(r chi.Router) {
		r.Use(jwtauth.Verifier(config.JWT()))
		r.Use(jwtauth.Authenticator)

		r.Get("/device", Get)
		r.Get("/device/{id}", GetFirst)
		r.Post("/device", Post)
		r.Put("/device/{id}", Put)
		r.Delete("/device/{id}", Delete)
	})

	return route
}
